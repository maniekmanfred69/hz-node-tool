Hz nood tool is application created to manage nodes (at this moment you can just check if they are online). It directly connects to server to check if it's online and get hallmark. Added servers are saved into "database.json" file, and loaded at tool start. Please tell me if you need tool like this, and if i should keep developing this tool (i was thinking about options like offline hallmark generating)


#Used third-party resources
* I used original java code (Hallmark) from Horizon source and I rewrote it to C#.
* Curve25519 class by Hans Wolff: https://github.com/hanswolff/curve25519
* FastRandom class by Colin Green: http://www.codeproject.com/Articles/9187/A-fast-equivalent-for-System-Random