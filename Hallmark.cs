﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace HorizonNodeMultiTool
{
    class Hallmark
    {
        private string hallmarkString;
        private byte[] publicKey;
        private string accountId;
        private byte[] signature;
        private string host;
        private int weight;
        private int date;
        private bool isValid;

        private Hallmark(string hallmarkString, byte[] publicKey, byte[] signature, string host, int weight, int date, bool isValid)
        {
            this.hallmarkString = hallmarkString;
            this.host = host;
            this.publicKey = publicKey;
            this.accountId = Account.getId(publicKey);
            //this.signature = signature;
            this.weight = weight;
            this.date = date;
            this.isValid = isValid;
        }

        public static Hallmark parseHallmark(string hallmarkString)
        {
            byte[] hallmarkBytes = Convert.parseHexString(hallmarkString);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(hallmarkBytes);
            MemoryStream buffer = new MemoryStream();
            buffer.Write(hallmarkBytes, 0, hallmarkBytes.Length);
            buffer.Position = 0;

            byte[] publicKey = new byte[32];
            buffer.Read(publicKey, 0, publicKey.Length);
            int hostLength = BitConverter.ToUInt16(buffer.GetBuffer(), System.Convert.ToInt32(buffer.Position));
            buffer.Position += 2;
            byte[] hostBytes = new byte[hostLength];
            buffer.Read(hostBytes, 0, hostBytes.Length);
            string host = System.Text.Encoding.UTF8.GetString(hostBytes);
            int weight = BitConverter.ToInt32(buffer.GetBuffer(), System.Convert.ToInt32(buffer.Position));
            int date = BitConverter.ToInt32(buffer.GetBuffer(), System.Convert.ToInt32(buffer.Position));

            return new Hallmark(hallmarkString, publicKey, hostBytes, host, weight, date, true);
        }

        public static string generateHallmark(string secretPhrase, string host, int weight, int date)
        {
            if (host.Length == 0 || host.Length > 100)
            {
                Log.Write("Hostname length should be between 1 and 100");
                return null;
            }
            if (weight <= 0 || weight > Constants.MAX_BALANCE_NHZ)
            {
                Log.Write("Weight should be between 1 and " + Constants.MAX_BALANCE_NHZ);
                return null;
            }

            byte[] publicKey = Crypto.getPublicKey(secretPhrase);
            byte[] hostBytes = System.Text.Encoding.UTF8.GetBytes(host);

            MemoryStream buffer = new MemoryStream();
            using (BinaryWriter writer = new BinaryWriter(buffer))
            {
                writer.Write(publicKey);
                writer.Write((short)hostBytes.Length);
                writer.Write(hostBytes);
                writer.Write((int)weight);
                writer.Write((int)date);
            }

            byte[] data = buffer.ToArray();
            return "XD";
        }

        public string getHallmarkString()
        {
            return this.hallmarkString;
        }

        public string getHost()
        {
            return this.host;
        }

        public int getWeight()
        {
            return this.weight;
        }

        public int getDate()
        {
            return this.date;
        }

        public string getPublicKey()
        {
            return Convert.toHexString(this.publicKey);
        }

        public string getAccountId()
        {
            return this.accountId;
        }
    }
}
