﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.IO;

namespace HorizonNodeMultiTool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            nodeList.View = View.Details;
            nodeList.LabelEdit = true;
            nodeList.AllowColumnReorder = true;
            nodeList.FullRowSelect = true;
            nodeList.GridLines = true;
            nodeList.Sorting = SortOrder.Ascending;

            nodeList.Columns.Add("IP", 85, HorizontalAlignment.Left);
            nodeList.Columns.Add("Status", 70, HorizontalAlignment.Left);
            nodeList.Columns.Add("Version", 70, HorizontalAlignment.Left);
            nodeList.Columns.Add("Public key", 160, HorizontalAlignment.Left);
            nodeList.Columns.Add("Account Id", 160, HorizontalAlignment.Left);
            Node.setReference(ref nodeList);
            Database.init();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void addNode_Click(object sender, EventArgs e)
        {
            addNodeForm form = new addNodeForm();
            form.Show();
        }

        private async void checkNodes_Click(object sender, EventArgs e)
        {
            foreach(ListViewItem item in nodeList.Items)
            {
                string ip = item.SubItems[0].Text;
                Task<bool> status = Node.checkNode(ip);
                bool _status = await status;
                if (_status == true)
                {
                    item.SubItems[1].Text = "ONLINE";
                    item.SubItems[1].BackColor = System.Drawing.Color.Green;
                }
                else
                {
                    item.SubItems[1].Text = "OFFLINE";
                    item.SubItems[1].BackColor = System.Drawing.Color.Red;
                }
            }
        }
    }
}
