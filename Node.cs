﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace HorizonNodeMultiTool
{
    class Node
    {
        private static ListView list;

        public static void setReference(ref ListView list)
        {
            Node.list = list;
        }

        public static ListView getReference()
        {
            return Node.list;
        }

        public static ListViewItem getListViewItemByIp(string ip)
        {
            foreach (ListViewItem item in Node.getReference().Items)
            {
                if (item.SubItems[0].Text == ip)
                    return item;
            }
            ListViewItem _item = new ListViewItem(ip);
            _item.UseItemStyleForSubItems = false;
            _item.SubItems.Add("WAITING");
            _item.SubItems.Add("");
            _item.SubItems.Add("");
            _item.SubItems.Add("");
            Node.getReference().Items.Add(_item);

            return _item;

        }

        public static async void addNode(string ip)
        {
            Task<string> response = getInfo(ip);
            string _response = await response;
            if (_response != null)
            {
                JObject json_object = (JObject)JsonConvert.DeserializeObject(_response);
                ListViewItem item = Node.getListViewItemByIp(ip);
                item.SubItems[1].Text = "ONLINE";
                item.SubItems[1].BackColor = System.Drawing.Color.Green;
                item.SubItems[2].Text = json_object["version"].ToString();
                if (json_object["hallmark"] != null)
                {
                    Hallmark hallmark = Hallmark.parseHallmark(json_object["hallmark"].ToString());
                    item.SubItems[3].Text = hallmark.getPublicKey();
                    item.SubItems[4].Text = hallmark.getAccountId();
                }
                Database.writeServer(ip);
            }
            else
            {
                ListViewItem item = Node.getListViewItemByIp(ip);
                item.SubItems[1].Text = "OFFLINE";
                item.SubItems[1].BackColor = System.Drawing.Color.Red;
                Database.writeServer(ip);
            }
        }

        public static async Task<bool> checkNode(string ip)
        {
            Task<string> response = getInfo(ip);
            string _response = await response;
            if (response != null)
            {
                return true;
            }
            return false;
        }

        private static async Task<string> getInfo(string ip)
        {
            string url = "http://" + ip + ":7774/nhz";
            JObject postData = JObject.FromObject(new
            {
                protocol = 1,
                requestType = "getInfo"
            });

            string response = await Task.Run(() => HttpManager.postRequest(url, postData.ToString()));
            return response;
        }
    }
}
