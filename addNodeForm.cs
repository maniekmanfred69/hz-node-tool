﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HorizonNodeMultiTool
{
    public partial class addNodeForm : Form
    {

        public addNodeForm()
        {
            InitializeComponent();
        }

        private void addNodeButton_Click(object sender, EventArgs e)
        {
            if (serverIpTextBox.Text != "")
            {
                if (serverIpTextBox.Text.Split(new Char[] { '.' }).Length == 4)
                {
                    ListViewItem item = new ListViewItem(serverIpTextBox.Text);
                    item.UseItemStyleForSubItems = false;
                    item.SubItems.Add("WAITING");
                    item.SubItems.Add("");
                    item.SubItems.Add("");
                    item.SubItems.Add("");
                    Node.getReference().Items.Add(item);
                    Node.addNode(serverIpTextBox.Text);
                }
                else
                    MessageBox.Show("Wrong IP format");
            }
            else
                MessageBox.Show("Fill server ip input!");
        }
    }
}
