﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HorizonNodeMultiTool
{
    static class Database
    {
        private static string databaseFile = "database.json";

        public static void init()
        {
            if (!File.Exists(databaseFile))
            {
                JArray serverArray = new JArray();

                using (FileStream fs = File.Open(databaseFile, FileMode.OpenOrCreate))
                using (StreamWriter sw = new StreamWriter(fs))
                using (JsonTextWriter writer = new JsonTextWriter(sw))
                {
                    serverArray.WriteTo(writer);
                }
            }

            List<string> servers = Database.returnServerList();
            foreach(string server in servers)
            {
                Node.addNode(server);
            }
        }

        public static void writeServer(string ip)
        {
            if (checkIfExists(ip))
                return;
            JsonSerializer serializer = new JsonSerializer();
            JArray database = JArray.Parse(File.ReadAllText(databaseFile));
            database.Add(ip);
            using (FileStream fs = File.Open(databaseFile, FileMode.Open))
            using (StreamWriter sw = new StreamWriter(fs))
            using (JsonWriter jw = new JsonTextWriter(sw))
            {
                jw.Formatting = Formatting.Indented;

                serializer.Serialize(jw, database);
            }
        }

        private static bool checkIfExists(string ip)
        {
            JArray database = JArray.Parse(File.ReadAllText(databaseFile));
            foreach (var item in database.Children())
            {
                if (item.ToString() == ip)
                    return true;
            }
            return false;
        }

        public static List<string> returnServerList()
        {
            JArray database = JArray.Parse(File.ReadAllText(databaseFile));
            List<string> serverList = new List<string>();
            foreach (var item in database.Children())
            {
                serverList.Add(item.ToString());
            }
            return serverList;
        }
    }
}
