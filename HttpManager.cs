﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Reflection;

namespace HorizonNodeMultiTool
{
    class HttpManager
    {
        public static CookieContainer cookies = new CookieContainer();

        public static string postRequest(string url, string postData)
        {
            try
            {
                HttpWebRequest PostRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                PostRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                PostRequest.CookieContainer = HttpManager.cookies;
                PostRequest.Timeout = 5000;
                PostRequest.Method = "POST";
                PostRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
                PostRequest.Headers["Accept-Encoding"] = "gzip,deflate,sdch";
                PostRequest.Headers["Accept-Language"] = "pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4";
                PostRequest.ContentLength = 0;
                PostRequest.ContentType = "application/json";
                PostRequest.AllowAutoRedirect = true;
                //PostRequest.Proxy = null;
                PostRequest.UserAgent = "HorizonNodeChecker";
                PostRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                var sp = PostRequest.ServicePoint;
                var prop = sp.GetType().GetProperty("HttpBehaviour",
                                        BindingFlags.Instance | BindingFlags.NonPublic);
                prop.SetValue(sp, (byte)0, null);
                PostRequest.ServicePoint.Expect100Continue = false;
 
                UTF8Encoding PostEncoding = new UTF8Encoding();
                byte[] PostloginDataBytes = PostEncoding.GetBytes(postData);
                PostRequest.ContentLength = PostloginDataBytes.Length;
                Stream stream = PostRequest.GetRequestStream();
                stream.Write(PostloginDataBytes, 0, PostloginDataBytes.Length);

                HttpWebResponse LoginResponse = (HttpWebResponse)PostRequest.GetResponse();
                using (StreamReader postRequestReader = new StreamReader(LoginResponse.GetResponseStream()))
                {
                    return postRequestReader.ReadToEnd();
                }
            }
            catch (ArgumentNullException)
            {
                //MessageBox.Show("Zła nazwa użytkownika lub hasło");
                return null;
            }
            catch (WebException)
            {
                //MessageBox.Show("Problem z połączeniem");
                return null;
            }
        }

        public static string getRequest(string url)
        {
            try
            {
                HttpWebRequest PostRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                PostRequest.CookieContainer = cookies;
                PostRequest.Method = "GET";
                PostRequest.Accept = "*/*";
                PostRequest.KeepAlive = true;
                PostRequest.Headers["X-Requested-With"] = "XMLHttpRequest";
                PostRequest.Headers["Accept-Encoding"] = "gzip, deflate, sdch";
                PostRequest.Headers["Accept-Language"] = "pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4";
                //PostRequest.Proxy = null;
                PostRequest.Headers["Cache-Control"] = "max-age=0";
                PostRequest.AllowAutoRedirect = true;
                PostRequest.UserAgent = "HorizonNodeChecker";
                PostRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                var sp = PostRequest.ServicePoint;
                var prop = sp.GetType().GetProperty("HttpBehaviour",
                                        BindingFlags.Instance | BindingFlags.NonPublic);
                prop.SetValue(sp, (byte)0, null);
                PostRequest.ServicePoint.Expect100Continue = false;

                HttpWebResponse LoginResponse = (HttpWebResponse)PostRequest.GetResponse();
                using (StreamReader postRequestReader = new StreamReader(LoginResponse.GetResponseStream()))
                {
                    return postRequestReader.ReadToEnd();
                }
            }
            catch (ArgumentNullException)
            {
                //MessageBox.Show("Zła nazwa użytkownika lub hasło");
                return null;
            }
            catch (WebException)
            {
                //MessageBox.Show("Problem z połączeniem");
                return null;
            }
        }
    }
}
