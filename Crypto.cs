﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace HorizonNodeMultiTool
{
    class Crypto
    {
        public static byte[] getPublicKey(string passphrase)

        {
            byte[] publicKey = new byte[32];
            Curve25519.KeyGenInline(publicKey, publicKey, HashAlgorithm.Create("SHA-256").ComputeHash(System.Text.Encoding.UTF8.GetBytes(passphrase)));
            return publicKey;
        }
    }
}
