﻿namespace HorizonNodeMultiTool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nodeList = new System.Windows.Forms.ListView();
            this.addNode = new System.Windows.Forms.Button();
            this.checkNodes = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // nodeList
            // 
            this.nodeList.Location = new System.Drawing.Point(12, 35);
            this.nodeList.Name = "nodeList";
            this.nodeList.Size = new System.Drawing.Size(608, 233);
            this.nodeList.TabIndex = 0;
            this.nodeList.UseCompatibleStateImageBehavior = false;
            this.nodeList.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // addNode
            // 
            this.addNode.Location = new System.Drawing.Point(12, 6);
            this.addNode.Name = "addNode";
            this.addNode.Size = new System.Drawing.Size(75, 23);
            this.addNode.TabIndex = 1;
            this.addNode.Text = "Add node";
            this.addNode.UseVisualStyleBackColor = true;
            this.addNode.Click += new System.EventHandler(this.addNode_Click);
            // 
            // checkNodes
            // 
            this.checkNodes.Location = new System.Drawing.Point(93, 6);
            this.checkNodes.Name = "checkNodes";
            this.checkNodes.Size = new System.Drawing.Size(86, 23);
            this.checkNodes.TabIndex = 2;
            this.checkNodes.Text = "Check nodes";
            this.checkNodes.UseVisualStyleBackColor = true;
            this.checkNodes.Click += new System.EventHandler(this.checkNodes_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 297);
            this.Controls.Add(this.checkNodes);
            this.Controls.Add(this.addNode);
            this.Controls.Add(this.nodeList);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView nodeList;
        private System.Windows.Forms.Button addNode;
        private System.Windows.Forms.Button checkNodes;
    }
}

