﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Math;

namespace HorizonNodeMultiTool
{
    class Convert
    {
        private static char[] hexChars = { '0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f' };

        public static byte[] parseHexString(string hex)
        {
            if (hex == null)
            {
                return null;
            }
            byte[] bytes = new byte[hex.Length / 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                int char1 = hex[i * 2];
                char1 = char1 > 0x60 ? char1 - 0x57 : char1 - 0x30;
                int char2 = hex[i * 2 + 1];
                char2 = char2 > 0x60 ? char2 - 0x57 : char2 - 0x30;
                if (char1 < 0 || char2 < 0 || char1 > 15 || char2 > 15)
                {
                    Console.Write("Invalid hex number: " + hex);
                }
                bytes[i] = (byte)((char1 << 4) + char2);
            }
            return bytes;
        }

        public static string toHexString(byte[] bytes)
        {
            if (bytes == null)
            {
                return null;
            }
            char[] chars = new char[bytes.Length * 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                chars[i * 2] = hexChars[((bytes[i] >> 4) & 0xF)];
                chars[i * 2 + 1] = hexChars[(bytes[i] & 0xF)];
            }
            return new string(chars);
        }

        public static string fullHashToId(byte[] hash)
        {
            if (hash == null || hash.Length < 8)
            {
                return null;
            }
            BigInteger bigInteger = new BigInteger(1, new byte[] { hash[7], hash[6], hash[5], hash[4], hash[3], hash[2], hash[1], hash[0] });
            return bigInteger.ToString();
        }
    }
}
