﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace HorizonNodeMultiTool
{
    class Account
    {
        public static string getId(byte[] publicKey)
        {
            byte[] publicKeyHash = HashAlgorithm.Create("SHA-256").ComputeHash(publicKey);
            return Convert.fullHashToId(publicKeyHash);
        }
    }
}
